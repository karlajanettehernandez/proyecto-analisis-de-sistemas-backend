package gt.umg.karlita.restaurante

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@SpringBootApplication
class RestApplication

@Configuration
@EnableWebMvc
class WebConfig : WebMvcConfigurer {

    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/**")
                .allowedHeaders("Content-Type", "Authorization", "authorization")
                .allowedMethods("GET", "POST", "PUT", "DELETE", "PATCH")
    }

}

fun main(args: Array<String>) {
    runApplication<RestApplication>(*args)
}
