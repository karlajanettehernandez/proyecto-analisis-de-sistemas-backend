package gt.umg.karlita.restaurante.controladores

import gt.umg.karlita.restaurante.consultas.ProductoTipoConsultas
import gt.umg.karlita.restaurante.models.ProductoTipoModel
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = ["/api/producto-tipo"])
class ProductoTipoController {

    @Autowired
    private lateinit var productoTipoConsultas: ProductoTipoConsultas

    @GetMapping
    fun obtenerProductoTipos(): ResponseEntity<List<ProductoTipoModel>> {

        val tipos = productoTipoConsultas.obtenerProductoTipos()

        return ResponseEntity.ok(tipos)

    }

}
