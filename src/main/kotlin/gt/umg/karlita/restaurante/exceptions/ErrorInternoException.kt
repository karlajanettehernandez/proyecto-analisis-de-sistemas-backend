package gt.umg.karlita.restaurante.exceptions

class ErrorInternoException(mensaje: String) : Exception(mensaje)

class AccesoNoAutorizadoException(mensaje: String = "Acceso no autorizado") : Exception(mensaje)