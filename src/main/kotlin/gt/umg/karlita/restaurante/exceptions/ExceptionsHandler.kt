package gt.umg.karlita.restaurante.exceptions

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody

@ControllerAdvice
class ExceptionsHandler {

    @ResponseBody
    @ExceptionHandler(ErrorInternoException::class)
    fun errorInternoExceptionHandler(ex: ErrorInternoException): ResponseEntity<Map<String, String>> {
        val respuesta = mutableMapOf<String, String>()

        respuesta["titulo"] = "Solicitud incorrecta"
        respuesta["mensaje"] = ex.message ?: "La solicitud necesita datos que son requeridos"

        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(respuesta)
    }

    @ResponseBody
    @ExceptionHandler(AccesoNoAutorizadoException::class)
    fun errorInternoExceptionHandler(ex: AccesoNoAutorizadoException): ResponseEntity<Map<String, String>> {
        val respuesta = mutableMapOf<String, String>()

        respuesta["titulo"] = "Acceso no autorizado"
        respuesta["mensaje"] = "No tiene autorizacion para acceder a este recurso"

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(respuesta)
    }

}
