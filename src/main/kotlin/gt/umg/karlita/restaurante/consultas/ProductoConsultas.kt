package gt.umg.karlita.restaurante.consultas

import gt.umg.karlita.restaurante.models.ProductoModel
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Repository
import java.math.BigDecimal
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Repository
class ProductoConsultas {

    @PersistenceContext
    @Qualifier
    private lateinit var entityManager: EntityManager

    fun crearProducto(producto: ProductoModel): Int {
        val sQuery = "insert into producto(activo, creado_por, fecha_creacion, precio, producto_descripcion, producto_nombre, producto_tipo_id, categoria_id) " +
                "      values(true, :creado_por, current_timestamp, :precio, :producto_descripcion, :producto_nombre, :producto_tipo_id, :categoria_id) returning id"

        val query = entityManager.createNativeQuery(sQuery)

        query.setParameter("creado_por", producto.creadoPor)
                .setParameter("precio", producto.precio)
                .setParameter("producto_descripcion", producto.productoDescripcion)
                .setParameter("producto_nombre", producto.productoNombre)
                .setParameter("producto_tipo_id", producto.productoTipoId)
                .setParameter("categoria_id", producto.categoriaId)

        val resultado = query.singleResult

        return resultado as Int
    }

    fun actualizarProducto(producto: ProductoModel) {
        val sQuery = "update producto " +
                "        set precio = :precio, " +
                "            producto_descripcion = :producto_descripcion, " +
                "            producto_nombre = :producto_nombre, " +
                "            producto_tipo_id = :producto_tipo_id, " +
                "            categoria_id = :categoria_id, " +
                "            activo = :activo " +
                "      where id = :id"

        val query = entityManager.createNativeQuery(sQuery)

        query.setParameter("precio", producto.precio)
                .setParameter("producto_descripcion", producto.productoDescripcion)
                .setParameter("producto_nombre", producto.productoNombre)
                .setParameter("producto_tipo_id", producto.productoTipoId)
                .setParameter("categoria_id", producto.categoriaId)
                .setParameter("activo", producto.activo)
                .setParameter("id", producto.id)

        query.executeUpdate()
    }

    fun obtenerProductosPorCategoria(categoriaId: Int): List<ProductoModel> {

        val sQuery = "select t0.id, " +
                "           t0.activo, " +
                "           t0.creado_por, " +
                "           t0.fecha_creacion, " +
                "           t0.precio, " +
                "           t0.producto_descripcion, " +
                "           t0.producto_nombre, " +
                "           t0.producto_tipo_id, " +
                "           t0.categoria_id " +
                "    from producto as t0 " +
                "      join categoria as t1 on t1.id = t0.categoria_id " +
                "      join producto_tipo as t2 on t2.id = t0.producto_tipo_id " +
                "    where t0.activo = true " +
                "      and t0.categoria_id = :categoria_id "

        val query = entityManager.createNativeQuery(sQuery)

        query.setParameter("categoria_id", categoriaId)

        return query
                .resultList
                .map { item ->
                    val data = item as Array<*>

                    return@map ProductoModel().apply {
                        this.id = data[0] as Int
                        this.activo = data[1] as Boolean
                        this.creadoPor = data[2] as String
                        this.precio = data[4] as BigDecimal
                        this.productoDescripcion = data[5] as String
                        this.productoNombre = data[6] as String
                        this.productoTipoId = data[7] as Int
                        this.categoriaId = data[8] as Int
                    }
                }
                .toList()

    }

}
