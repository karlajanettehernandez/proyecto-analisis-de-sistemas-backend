package gt.umg.karlita.restaurante.consultas

import gt.umg.karlita.restaurante.models.ProductoTipoModel
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Repository
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Repository
class ProductoTipoConsultas {

    @PersistenceContext
    @Qualifier
    private lateinit var entityManager: EntityManager

    fun obtenerProductoTipos(): List<ProductoTipoModel> {

        val sQuery = "select id, activo, creado_por, fecha_creacion, producto_tipo_nombre " +
                "      from producto_tipo " +
                "      where activo = true " +
                "      order by producto_tipo_nombre asc"

        val query = entityManager.createNativeQuery(sQuery)

        return query
                .resultList
                .map { item ->
                    val data = item as Array<*>

                    return@map ProductoTipoModel().apply {
                        this.id = data[0] as Int
                        this.activo = data[1] as Boolean
                        this.creadoPor = data[2] as String
                        this.productoTipoNombre = data[4] as String
                    }
                }
                .toList()
    }

}
